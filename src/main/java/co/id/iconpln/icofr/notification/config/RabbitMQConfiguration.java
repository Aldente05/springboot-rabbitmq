package co.id.iconpln.icofr.notification.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.util.List;

/**
 * Created by f.putra on 10/02/18.
 */
@Configuration
@ComponentScan(basePackages = {"co.id.iconpln.icofr.notification.common", "co.id.iconpln.icofr.notification.service"})
@EnableWebSocketMessageBroker
public class RabbitMQConfiguration implements WebSocketMessageBrokerConfigurer {

    /**
     * Register Stomp endpoints: the url to open the WebSocket connection.
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws")
                .setAllowedOrigins("http://localhost:8088")
                .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableStompBrokerRelay("/queue/", "/topic/")
                .setRelayHost("")
                .setRelayPort(61613)
                .setClientLogin("")
                .setClientPasscode("")
                .setSystemLogin("")
                .setSystemPasscode("")
                .setAutoStartup(true);
        registry.setApplicationDestinationPrefixes("/app");
    }
}
