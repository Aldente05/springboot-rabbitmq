package co.id.iconpln.icofr.notification.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by f.putra on 17/01/18.
 */
@Getter
@Setter
@ToString
public class Notification {

    private Message message;
    private String to;
    private String type;
}

