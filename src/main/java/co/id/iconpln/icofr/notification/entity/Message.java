package co.id.iconpln.icofr.notification.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by f.putra on 17/01/18.
 */
@Getter
@Setter
public class Message {

    private String title;
    private String body;
}
