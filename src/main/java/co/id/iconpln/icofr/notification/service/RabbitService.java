package co.id.iconpln.icofr.notification.service;

import co.id.iconpln.icofr.notification.entity.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by f.putra on 05/03/18.
 */
@Service
public class RabbitService {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    public void sendMessage (Notification notification){
        try{
            simpMessagingTemplate.convertAndSend("/topic/berita", notification);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
