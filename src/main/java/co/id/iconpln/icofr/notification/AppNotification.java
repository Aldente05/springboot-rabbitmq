package co.id.iconpln.icofr.notification;

import co.id.iconpln.icofr.notification.config.RabbitMQConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Created by f.putra on 10/02/18.
 */
@SpringBootApplication
@Import({RabbitMQConfiguration.class})
public class AppNotification {}
